#!/bin/sh

docker build -t mbaum2000/build:base base
docker build -t mbaum2000/build:qt4.8 qt4.8
docker build -t mbaum2000/build:qt5.5 qt5.5
docker build -t mbaum2000/build:qt5.9 qt5.9

docker build -t mbaum2000/build-i386:base base-i386
docker build -t mbaum2000/build-i386:qt4.8 qt4.8-i386
docker build -t mbaum2000/build-i386:qt5.5 qt5.5-i386
docker build -t mbaum2000/build-i386:qt5.9 qt5.9-i386

docker build -t mbaum2000/build-mxe:base base-mxe
docker build -t mbaum2000/build-mxe:qt5 mxe-qt5

